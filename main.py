from flask import Flask
from flask import json
import requests

# If `entrypoint` is not defined in app.yaml, App Engine will look for an app
# called `app` in `main.py`.
app = Flask(__name__)

@app.route('/')
def hello():
    return 'First Page'

@app.route('/departments')
def departments():
    """Return a friendly HTTP greeting."""
    response = requests.get('https://ultra-automata-237814.appspot.com/departments')
    return response.content
    
@app.route('/departments/employees')
def employeePerDepartment():
    response = requests.get('https://ultra-automata-237814.appspot.com/departments/employees')
    return response.text
    
@app.route('/employees')
def employees():
    res = requests.get('https://ultra-automata-237814.appspot.com/employees')
    return app.response_class(
        response = json.dumps(res.json()),
        status = 200,
        mimetype = 'application/json'
    )

@app.route('/employees/gender/female')
def employeeFemale():
    response = requests.get('https://ultra-automata-237814.appspot.com/employees/gender/female')
    female = json.dumps(response.json())
    return female
    
@app.route('/employees/gender/male')
def employeeMale():
    response = requests.get('https://ultra-automata-237814.appspot.com/employees/gender/male')
    return response
    
    
@app.route('/salaries/top/10')
def topSalaries():
    response = requests.get('https://ultra-automata-237814.appspot.com/salaries/top/10')
    return response.text
    
@app.route('/titles')
def titles():
    response = requests.get('https://ultra-automata-237814.appspot.com/titles')
    return response.text

@app.route('/todos')
def todos():
    response = requests.get("https://jsonplaceholder.typicode.com/todos")
    return response

if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
    
